package com.lookren.blackjack;

import com.lookren.blackjack.model.Card;

import java.util.List;

public class MockPlayerHand extends PlayerHand {
    public MockPlayerHand(List<Card> cards) {
        for (Card card : cards) {
            addCard(card);
        }
    }
}
