package com.lookren.blackjack;

import com.lookren.blackjack.model.CardType;

public class CalculateValuesTest extends AbstractBlackJackTestCase {

    public void testCalculateValueLargeList() {
        int amount = 25;
        int expectedResult = 8 * amount;
        generateCards(amount, CardType.EIGHT);
        checkCardListValue(cards, expectedResult);
    }

    public void testCalculateValueBlackJackVariants() {
        int expectedResult = 21;

        cards.add(createCard(CardType.ACE));
        cards.add(createCard(CardType.KING));
        checkCardListValue(cards, expectedResult);
        cards.clear();

        cards.add(createCard(CardType.ACE));
        cards.add(createCard(CardType.QUEEN));
        checkCardListValue(cards, expectedResult);
        cards.clear();

        cards.add(createCard(CardType.ACE));
        cards.add(createCard(CardType.KNAVE));
        checkCardListValue(cards, expectedResult);
        cards.clear();

        cards.add(createCard(CardType.ACE));
        cards.add(createCard(CardType.TEN));
        checkCardListValue(cards, expectedResult);
    }

    public void testCalculateValueAceVariants() {
        int expectedResult = 20;
        cards.add(createCard(CardType.ACE));
        cards.add(createCard(CardType.NINE));
        checkCardListValue(cards, expectedResult);
        cards.clear();

        expectedResult = 21;
        cards.add(createCard(CardType.ACE));
        cards.add(createCard(CardType.NINE));
        cards.add(createCard(CardType.ACE));
        checkCardListValue(cards, expectedResult);
        cards.clear();

        expectedResult = 12;
        cards.add(createCard(CardType.ACE));
        cards.add(createCard(CardType.NINE));
        cards.add(createCard(CardType.ACE));
        cards.add(createCard(CardType.ACE));
        checkCardListValue(cards, expectedResult);
        cards.clear();

        expectedResult = 22;
        cards.add(createCard(CardType.ACE));
        cards.add(createCard(CardType.NINE));
        cards.add(createCard(CardType.ACE));
        cards.add(createCard(CardType.ACE));
        cards.add(createCard(CardType.KING));
        checkCardListValue(cards, expectedResult);
    }

    public void testCalculateEachValue() {
        int expectedResult = 2+3+4+5+6+7+8+9+10*4+1;
        cards.add(createCard(CardType.TWO));
        cards.add(createCard(CardType.THREE));
        cards.add(createCard(CardType.FOUR));
        cards.add(createCard(CardType.FIVE));
        cards.add(createCard(CardType.SIX));
        cards.add(createCard(CardType.SEVEN));
        cards.add(createCard(CardType.EIGHT));
        cards.add(createCard(CardType.NINE));
        cards.add(createCard(CardType.TEN));
        cards.add(createCard(CardType.KNAVE));
        cards.add(createCard(CardType.QUEEN));
        cards.add(createCard(CardType.KING));
        cards.add(createCard(CardType.ACE));
        checkCardListValue(cards, expectedResult);
    }

    public void testCalculateHandValue() {
        cards.add(createCard(CardType.ACE));
        cards.add(createCard(CardType.EIGHT));
        cards.add(createCard(CardType.SEVEN));
        PlayerHand playerHand = new MockPlayerHand(cards);

        int expectedResult = 16;
        checkPlayHandValue(playerHand, expectedResult);
        cards.clear();

        cards.add(createCard(CardType.ACE));
        cards.add(createCard(CardType.KING));
        playerHand = new MockPlayerHand(cards);

        expectedResult = 21;
        checkPlayHandValue(playerHand, expectedResult);
        playerHand.addCard(createCard(CardType.TWO));
        expectedResult = 13;
        checkPlayHandValue(playerHand, expectedResult);
        playerHand.addCard(createCard(CardType.EIGHT));
        expectedResult = 21;
        checkPlayHandValue(playerHand, expectedResult);
        playerHand.addCard(createCard(CardType.THREE));
        expectedResult = 24;
        checkPlayHandValue(playerHand, expectedResult);
    }
}
