package com.lookren.blackjack;

import com.lookren.blackjack.model.Card;

import java.util.List;

public class MockBlackJackDeck extends BlackJackDeck {

    public MockBlackJackDeck(List<Card> cards) {
        super(1);
        mCards = cards;
    }
}
