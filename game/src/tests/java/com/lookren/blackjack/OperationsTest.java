package com.lookren.blackjack;

import com.lookren.blackjack.model.CardType;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class OperationsTest extends AbstractBlackJackTestCase {

    private ByteArrayInputStream inputContent;
    private static final int INIT_CARDS_OUT = 4;

    public void testPlayGameSuccess() throws IOException {
        int deckSize = 20;
        int balance = 100;
        generateCards(deckSize, CardType.TEN);
        BlackJackDeck deck = new MockBlackJackDeck(cards);
        checkTestValue(deckSize, deck.mCards.size(), "deck size");

        String input = String.valueOf(BlackJackOperation.STAND.getValue());

        inputContent = new ByteArrayInputStream(input.getBytes());
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputContent));

        BlackJackPlayField playField = new BlackJackPlayField();
        playField.playGame(balance, deck, bufferedReader);

        checkTestValue(deckSize - INIT_CARDS_OUT, deck.mCards.size(), "deck size");
        checkTestValue(2, playField.mDealerCards.size(), "dealer cards amount");
        checkTestValue(1, playField.mPlayerHands.size(), "play hands amount");
        checkTestValue(Game.INIT_BET, playField.mPlayerHands.get(0).getBet(), "bet");
        checkTestValue(20, playField.mPlayerHands.get(0).calculateValue(), "player value");
        int dealerValue = BlackJackUtils.calculateValue(playField.mDealerCards);
        checkTestValue(20, dealerValue, "dealer value");
        checkTestValue(Game.INIT_BET, playField.calculateAllHandsResults(dealerValue, false), "game result");
    }

    public void testPlayGameDealerBlackAndJackPlayerBlackJack() throws IOException {
        int balance = 100;
        cards.add(createCard(CardType.ACE));
        cards.add(createCard(CardType.TEN));
        cards.add(createCard(CardType.KING));
        cards.add(createCard(CardType.ACE));
        cards.add(createCard(CardType.SIX));
        cards.add(createCard(CardType.QUEEN));
        cards.add(createCard(CardType.QUEEN));
        cards.add(createCard(CardType.QUEEN));
        BlackJackDeck deck = new MockBlackJackDeck(cards);
        int deckSize = cards.size();
        checkTestValue(deckSize, deck.mCards.size(), "deck size");

        inputContent = new ByteArrayInputStream("".getBytes());
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputContent));

        BlackJackPlayField playField = new BlackJackPlayField();
        playField.playGame(balance, deck, bufferedReader);

        checkTestValue(deckSize - INIT_CARDS_OUT, deck.mCards.size(), "deck size");
        checkTestValue(2, playField.mDealerCards.size(), "dealer cards amount");
        checkTestValue(1, playField.mPlayerHands.size(), "play hands amount");
        checkTestValue(Game.INIT_BET * PlayerHand.BLACK_JACK_RATIO, playField.mPlayerHands.get(0).getBet(), "bet");
        checkTestValue(BlackJackUtils.BLACK_JACK_VALUE, playField.mPlayerHands.get(0).calculateValue(), "player value");
        int dealerValue = BlackJackUtils.calculateValue(playField.mDealerCards);
        checkTestValue(BlackJackUtils.BLACK_JACK_VALUE, dealerValue, "dealer value");
        checkTestValue(0, playField.calculateAllHandsResults(dealerValue, true), "game result");
    }

    public void testPlayGameDealerBlackJack() throws IOException {
        int balance = 100;
        cards.add(createCard(CardType.FIVE));
        cards.add(createCard(CardType.ACE));
        cards.add(createCard(CardType.TEN));
        cards.add(createCard(CardType.TEN));
        cards.add(createCard(CardType.SIX));
        cards.add(createCard(CardType.QUEEN));
        cards.add(createCard(CardType.QUEEN));
        cards.add(createCard(CardType.QUEEN));
        cards.add(createCard(CardType.QUEEN));
        BlackJackDeck deck = new MockBlackJackDeck(cards);
        int deckSize = cards.size();
        checkTestValue(deckSize, deck.mCards.size(), "deck size");

        String input = String.valueOf(BlackJackOperation.HIT.getValue());

        inputContent = new ByteArrayInputStream(input.getBytes());
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputContent));

        BlackJackPlayField playField = new BlackJackPlayField();
        playField.playGame(balance, deck, bufferedReader);

        checkTestValue(deckSize - INIT_CARDS_OUT - 1, deck.mCards.size(), "deck size");
        checkTestValue(2, playField.mDealerCards.size(), "dealer cards amount");
        checkTestValue(1, playField.mPlayerHands.size(), "play hands amount");
        checkTestValue(Game.INIT_BET, playField.mPlayerHands.get(0).getBet(), "bet");
        checkTestValue(21, playField.mPlayerHands.get(0).calculateValue(), "player value");
        int dealerValue = BlackJackUtils.calculateValue(playField.mDealerCards);
        checkTestValue(BlackJackUtils.BLACK_JACK_VALUE, dealerValue, "dealer value");
        checkTestValue(-Game.INIT_BET, playField.calculateAllHandsResults(dealerValue, true), "game result");
    }

    public void testPlayGameFail() throws IOException {
        int balance = 100;
        cards.add(createCard(CardType.THREE));
        cards.add(createCard(CardType.FOUR));
        cards.add(createCard(CardType.FIVE));
        cards.add(createCard(CardType.SIX));
        cards.add(createCard(CardType.SEVEN));
        cards.add(createCard(CardType.EIGHT));
        cards.add(createCard(CardType.NINE));
        cards.add(createCard(CardType.TEN));
        cards.add(createCard(CardType.KNAVE));
        cards.add(createCard(CardType.QUEEN));
        cards.add(createCard(CardType.KING));
        int deckSize = cards.size();
        BlackJackDeck deck = new MockBlackJackDeck(cards);
        assertTrue("deck size incorrect " + deck.mCards.size(), deck.mCards.size() == deckSize);
        String input =
                String.valueOf(BlackJackOperation.HIT.getValue()) + "\n"
                + String.valueOf(BlackJackOperation.HIT.getValue()) + "\n"
                + String.valueOf(BlackJackOperation.STAND.getValue());

        inputContent = new ByteArrayInputStream(input.getBytes());
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputContent));
        BlackJackPlayField playField = new BlackJackPlayField();
        playField.playGame(balance, deck, bufferedReader);

        checkTestValue(deckSize - INIT_CARDS_OUT - 2, deck.mCards.size(), "deck size");
        checkTestValue(2, playField.mDealerCards.size(), "dealer cards amount");
        checkTestValue(1, playField.mPlayerHands.size(), "play hands amount");
        checkTestValue(Game.INIT_BET, playField.mPlayerHands.get(0).getBet(), "bet");
        checkTestValue(23, playField.mPlayerHands.get(0).calculateValue(), "player value");
        checkPlayHand(playField.mPlayerHands.get(0), false, true, false, true, false, false, false, false);
        int dealerValue = BlackJackUtils.calculateValue(playField.mDealerCards);
        checkTestValue(10, dealerValue, "dealer value");
        checkTestValue(-Game.INIT_BET, playField.calculateAllHandsResults(dealerValue, false), "game result");
    }

    public void testPlayGameSplit() throws IOException {
        int deckSize = 20;
        int balance = 100;
        generateCards(deckSize, CardType.TEN);
        BlackJackDeck deck = new MockBlackJackDeck(cards);
        checkTestValue(deckSize, deck.mCards.size(), "deck size");

        String input =
                String.valueOf(BlackJackOperation.SPLIT.getValue()) + "\n" +
                String.valueOf(BlackJackOperation.HIT.getValue()) + "\n" +
                String.valueOf(BlackJackOperation.STAND.getValue()) + "\n";

        inputContent = new ByteArrayInputStream(input.getBytes());
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputContent));

        BlackJackPlayField playField = new BlackJackPlayField();
        playField.playGame(balance, deck, bufferedReader);

        checkTestValue(deckSize - INIT_CARDS_OUT - 2 - 1, deck.mCards.size(), "deck size");
        checkTestValue(2, playField.mDealerCards.size(), "dealer cards amount");
        checkTestValue(2, playField.mPlayerHands.size(), "play hands amount");
        checkTestValue(Game.INIT_BET, playField.mPlayerHands.get(0).getBet(), "hand#1 bet");
        checkTestValue(Game.INIT_BET, playField.mPlayerHands.get(1).getBet(), "hand#2 bet");
        checkTestValue(30, playField.mPlayerHands.get(0).calculateValue(), "player hand#1 value");
        checkTestValue(20, playField.mPlayerHands.get(1).calculateValue(), "player hand#2 value");
        int dealerValue = BlackJackUtils.calculateValue(playField.mDealerCards);
        checkTestValue(20, dealerValue, "dealer value");
        checkTestValue(0, playField.calculateAllHandsResults(dealerValue, false), "game result");
    }
}
