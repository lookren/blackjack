package com.lookren.blackjack;

import com.lookren.blackjack.model.Card;
import com.lookren.blackjack.model.CardType;
import com.lookren.blackjack.model.Suit;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

public class AbstractBlackJackTestCase extends TestCase {
    List<Card> cards = new ArrayList<>();

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        cards.clear();
    }

    protected void generateCards(int amount, CardType cardType) {
        for (int i = 0; i < amount; i++) {
            cards.add(createCard(cardType));
        }
    }

    protected Card createCard(CardType cardType) {
        return new Card(cardType, Suit.CLUBS);
    }

    protected void checkCardListValue(List<Card> cards, int expectedValue) {
        assertTrue("Cards value should be " + expectedValue + " but " + BlackJackUtils.calculateValue(cards),
                BlackJackUtils.calculateValue(cards) == expectedValue);
    }

    protected void checkPlayHandValue(PlayerHand playerHand, int expectedValue) {
        assertTrue("Player Hand value should be " + expectedValue + " but " + playerHand.calculateValue(),
                playerHand.calculateValue() == expectedValue);
    }

    protected void checkPlayHand(PlayerHand playerHand, boolean active, boolean hittable, boolean canDoubleDown,
                                 boolean bust, boolean blackjack, boolean wait, boolean surrender, boolean canSplit) {
        assertTrue("active should be " + active, playerHand.isActive() == active);
        assertTrue("hittable should be " + hittable, playerHand.canHit() == hittable);
        assertTrue("can double down should be " + canDoubleDown, playerHand.canDoubleDown() == canDoubleDown);
        assertTrue("can split should be " + canSplit, playerHand.canSplit() == canSplit);
        assertTrue("bust should be " + bust, playerHand.isBust() == bust);
        assertTrue("blackjack should be " + blackjack, playerHand.isBlackJack() == blackjack);
        assertTrue("surrender should be " + surrender, playerHand.isSurrender() == surrender);
        assertTrue("wait should be " + wait, playerHand.isWait() == wait);
    }

    protected void checkTestValue(int expected, int actual, String comment) {
        assertTrue("incorrect value " + comment + ": expected = " + expected + ", actual = " + actual, actual == expected);
    }

    protected void checkTestValue(float expected, float actual, String comment) {
        assertTrue("incorrect value " + comment + ": expected = " + expected + ", actual = " + actual, actual == expected);
    }

    protected void checkTestValue(String expected, String actual, String comment) {
        assertTrue("incorrect value " + comment + ": expected = " + expected + ", actual = " + actual,
                /*for null values*/ actual == expected || actual.equals(expected));
    }
}
