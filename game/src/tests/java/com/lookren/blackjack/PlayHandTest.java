package com.lookren.blackjack;

import com.lookren.blackjack.model.Card;
import com.lookren.blackjack.model.CardType;

import java.util.ArrayList;
import java.util.List;

public class PlayHandTest extends AbstractBlackJackTestCase {

    public void testFlagsActiveHand() {
        List<Card> cards = new ArrayList<>();
        cards.add(createCard(CardType.ACE));
        cards.add(createCard(CardType.TWO));

        PlayerHand playerHand = new MockPlayerHand(cards);
        int expectedResult = 13;
        checkPlayHandValue(playerHand, expectedResult);
        checkPlayHand(playerHand, true, true, true, false, false, false, false, false);

        playerHand.addCard(createCard(CardType.THREE));
        expectedResult = 16;
        checkPlayHandValue(playerHand, expectedResult);
        checkPlayHand(playerHand, true, true, false, false, false, false, false, false);

        playerHand.addCard(createCard(CardType.TEN));
        expectedResult = 16;
        checkPlayHandValue(playerHand, expectedResult);
        checkPlayHand(playerHand, true, true, false, false, false, false, false, false);

        playerHand.addCard(createCard(CardType.TEN));
        expectedResult = 26;
        checkPlayHandValue(playerHand, expectedResult);
        checkPlayHand(playerHand, false, true, false, true, false, false, false, false);
    }

    public void testFlagsBlackJack() {
        List<Card> cards = new ArrayList<>();
        cards.add(createCard(CardType.ACE));
        cards.add(createCard(CardType.TEN));

        PlayerHand playerHand = new MockPlayerHand(cards);
        checkPlayHandValue(playerHand, 21);
        checkPlayHand(playerHand, false, true, true, false, true, false, false, false);
    }

    public void testFlagsNotBlackJack() {
        List<Card> cards = new ArrayList<>();
        cards.add(createCard(CardType.ACE));
        cards.add(createCard(CardType.FIVE));
        cards.add(createCard(CardType.FIVE));

        PlayerHand playerHand = new MockPlayerHand(cards);
        checkPlayHandValue(playerHand, 21);
        checkPlayHand(playerHand, false, true, false, false, false, false, false, false);
    }

    public void testFlagsSplit() {
        List<Card> cards = new ArrayList<>();
        cards.add(createCard(CardType.TEN));
        cards.add(createCard(CardType.TEN));

        PlayerHand playerHand = new MockPlayerHand(cards);
        checkPlayHandValue(playerHand, 20);
        checkPlayHand(playerHand, true, true, true, false, false, false, false, true);
    }

    public void testFlagsAceSplit() {
        List<Card> cards = new ArrayList<>();
        cards.add(createCard(CardType.ACE));
        cards.add(createCard(CardType.ACE));

        PlayerHand playerHand = new MockPlayerHand(cards);
        checkPlayHandValue(playerHand, 12);
        checkPlayHand(playerHand, true, true, true, false, false, false, false, true);
    }

    public void testFlagsSurrender() {
        List<Card> cards = new ArrayList<>();
        cards.add(createCard(CardType.TEN));
        cards.add(createCard(CardType.FOUR));

        PlayerHand playerHand = new MockPlayerHand(cards);
        checkPlayHandValue(playerHand, 14);
        checkPlayHand(playerHand, true, true, true, false, false, false, false, false);
        playerHand.surrender();
        checkPlayHand(playerHand, false, true, true, false, false, false, true, false);
    }

    public void testFlagsStand() {
        List<Card> cards = new ArrayList<>();
        cards.add(createCard(CardType.TEN));
        cards.add(createCard(CardType.SEVEN));

        PlayerHand playerHand = new MockPlayerHand(cards);
        checkPlayHandValue(playerHand, 17);
        checkPlayHand(playerHand, true, true, true, false, false, false, false, false);
        playerHand.stand();
        checkPlayHand(playerHand, false, true, true, false, false, true, false, false);
    }
}
