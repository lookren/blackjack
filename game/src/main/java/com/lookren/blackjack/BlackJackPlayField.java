package com.lookren.blackjack;

import com.lookren.blackjack.model.Card;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BlackJackPlayField {
    private static final int DEALER_MAX_VALUE = 17;

    int mPlayerBalance;
    List<Card> mDealerCards = new ArrayList<>();
    List<PlayerHand> mPlayerHands = new ArrayList<>();

    public int playGame(int playerBalance, BlackJackDeck deck, BufferedReader in) throws IOException {
        initGame(playerBalance, deck);
        int dealerValue = BlackJackUtils.getDealerValue(mDealerCards, false);
        boolean dealerBlackJack = dealerValue == BlackJackUtils.BLACK_JACK_VALUE;
        boolean start = true;

        if (dealerBlackJack && !hasActivePlayerHands()) {
            BlackJackUtils.printGameFieldInfo(mDealerCards, mPlayerHands, true, true);
        }
        while (hasActivePlayerHands()) {
            int splitNeededForPlayHandIndex = -1;
            for (int playHandIndex = 0; playHandIndex < mPlayerHands.size(); playHandIndex++) {
                PlayerHand playerHand = mPlayerHands.get(playHandIndex);
                if (!playerHand.isActive()) {
                    continue;
                }
                BlackJackUtils.printGameFieldInfo(mDealerCards, mPlayerHands, true, start);
                start = false;
                BlackJackOperation chosenOperation = BlackJackUtils.getEnteredOperation(in,
                        playerHand.getAvailableOperations(mPlayerBalance), mPlayerHands.size() > 1, playHandIndex);
                if (chosenOperation == BlackJackOperation.SPLIT) {
                    splitNeededForPlayHandIndex = playHandIndex;
                    break;
                } else {
                    playerHand.process(chosenOperation, deck);
                }
            }
            if (splitNeededForPlayHandIndex >= 0) {
                mPlayerHands.add(mPlayerHands.get(splitNeededForPlayHandIndex).split(deck.getCard(), deck.getCard()));
            }
            if (dealerBlackJack) {
                break;
            }
        }
        if (isWaitingForDealer()) {
            while (BlackJackUtils.getDealerValue(mDealerCards, false) < DEALER_MAX_VALUE) {
                mDealerCards.add(deck.getCard());
            }
        }
        BlackJackUtils.printGameFieldInfo(mDealerCards, mPlayerHands, false, false);
        dealerValue = BlackJackUtils.getDealerValue(mDealerCards, false);
        int result = calculateAllHandsResults(dealerValue, dealerBlackJack);
        BlackJackUtils.printResult(result);

        return result;
    }

    private void initGame(int playerBalance, BlackJackDeck deck) {
        mPlayerBalance = playerBalance;
        PlayerHand firstPlayerHand = new PlayerHand();
        mPlayerHands.add(firstPlayerHand);
        firstPlayerHand.addCard(deck.getCard());
        mDealerCards.add(deck.getCard());
        firstPlayerHand.addCard(deck.getCard());
        mDealerCards.add(deck.getCard());
    }

    int calculateAllHandsResults(int dealerValue, boolean dealerBlackJack) {
        int result = 0;
        for (PlayerHand playerHand : mPlayerHands) {
            if (dealerBlackJack) {
                if(!playerHand.isBlackJack()) {
                    result -= playerHand.getBet();
                }
            } else {
                if (playerHand.isBlackJack()) {
                    result += playerHand.getBet();
                } else if (playerHand.isSurrender() || playerHand.isBust()) {
                    result -= playerHand.getBet();
                } else if (playerHand.calculateValue() == BlackJackUtils.BLACK_JACK_VALUE
                        || playerHand.calculateValue() >= dealerValue
                        || dealerValue > BlackJackUtils.BLACK_JACK_VALUE) {
                    result += playerHand.getBet();
                } else {
                    result -= playerHand.getBet();
                }
            }
        }
        return result;
    }

    private boolean hasActivePlayerHands() {
        for (PlayerHand playerHand : mPlayerHands) {
            if (playerHand.isActive()) {
                return true;
            }
        }
        return false;
    }

    private boolean isWaitingForDealer() {
        for (PlayerHand playerHand : mPlayerHands) {
            if (playerHand.isWait()) {
                return true;
            }
        }
        return false;
    }
}

