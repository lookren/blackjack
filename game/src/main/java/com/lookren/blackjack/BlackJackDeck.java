package com.lookren.blackjack;

import com.lookren.blackjack.model.Card;
import com.lookren.blackjack.model.CardType;
import com.lookren.blackjack.model.Suit;

import java.lang.IllegalArgumentException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BlackJackDeck {
    private static final int DECK_SIZE = CardType.values().length * Suit.values().length;

    protected List<Card> mCards = new ArrayList<>();

    public BlackJackDeck(int deckAmount) {
        for (int i = 0; i < deckAmount; i++) {
            initializeDeck();
        }
    }

    public Card getCard() {
        return mCards.remove(0);
    }

    public boolean isReadyForGame() {
        return mCards.size() > DECK_SIZE / 3;
    }

    private void initializeDeck() {
        for (int i = 0; i < DECK_SIZE; i++) {
            CardType cardType;
            Suit suit;
            int suitSize = DECK_SIZE / Suit.values().length;
            if (i < suitSize) {
                suit = Suit.HEARTS;
                cardType = getCardType(i);
            } else if (i < suitSize * 2) {
                suit = Suit.SPADES;
                cardType = getCardType(i - suitSize);
            } else if (i < suitSize * 3) {
                suit = Suit.DIAMONDS;
                cardType = getCardType(i - suitSize * 2);
            } else {
                suit = Suit.CLUBS;
                cardType = getCardType(i - suitSize * 3);
            }

            Card card = new Card(cardType, suit);
            mCards.add(card);
        }

        Collections.shuffle(mCards);
    }

    private CardType getCardType(int number) {
        switch (number) {
            case 0: return CardType.TWO;
            case 1: return CardType.THREE;
            case 2: return CardType.FOUR;
            case 3: return CardType.FIVE;
            case 4: return CardType.SIX;
            case 5: return CardType.SEVEN;
            case 6: return CardType.EIGHT;
            case 7: return CardType.NINE;
            case 8: return CardType.TEN;
            case 9: return CardType.KNAVE;
            case 10: return CardType.QUEEN;
            case 11: return CardType.KING;
            case 12: return CardType.ACE;
            default:
                throw new IllegalArgumentException("number should be from 0 to 12: " + number);
        }
    }
}