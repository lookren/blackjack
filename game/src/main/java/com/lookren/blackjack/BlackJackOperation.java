package com.lookren.blackjack;

enum BlackJackOperation {
    HIT(1, "Hit"), STAND(2, "Stand"), DOUBLE_DOWN(3, "Double Down"),
    SPLIT(4, "Split"), SURRENDER(5, "Surrender");

    private int mDigit;
    private String mText;

    BlackJackOperation(int digit, String text) {
        mDigit = digit;
        mText = text;
    }

    public int getValue() {
        return mDigit;
    }

    @Override
    public String toString() {
        return "(" + mDigit + ") " + mText + " ";
    }
}
