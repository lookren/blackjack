package com.lookren.blackjack;

import com.lookren.blackjack.model.Card;
import com.lookren.blackjack.model.CardType;

import java.util.ArrayList;
import java.util.List;

class PlayerHand {
    static final float BLACK_JACK_RATIO = 1.5f;
    private static final float DOUBLE_RATIO = 2f;

    private List<Card> mCards = new ArrayList<>();
    private int mBet = Game.INIT_BET;
    private boolean mBlackJack;
    private boolean mWait;
    private boolean mSurrender;
    private boolean mHittable;

    void addCard(Card card) {
        mCards.add(card);
        if (mCards.size() == 2 && calculateValue() == BlackJackUtils.BLACK_JACK_VALUE) {
            mBlackJack = true;
            mBet *= BLACK_JACK_RATIO;
        }
    }

    PlayerHand split(Card card1, Card card2) {
        if (!justInitialized()) {
            throw new UnsupportedOperationException("cannot split");
        }
        PlayerHand newHand = new PlayerHand();
        Card cardToMove = mCards.remove(1);
        newHand.addCard(cardToMove);
        mCards.add(card1);
        newHand.addCard(card2);
        mHittable = cardToMove.mCardType == CardType.ACE;
        newHand.mHittable = mHittable;
        return newHand;
    }

    void doubleDown(Card card) {
        mBet *= DOUBLE_RATIO;
        addCard(card);
        mWait = true;
    }

    void stand() {
        mWait = true;
    }

    boolean canHit() {
        return !mHittable;
    }

    boolean canDoubleDown() {
        return canHit() && mCards.size() == 2;
    }

    boolean canSplit() {
        return justInitialized() &&
                BlackJackUtils.getValue(mCards.get(0).mCardType)
                        == BlackJackUtils.getValue(mCards.get(1).mCardType);
    }

    boolean isBlackJack() {
        return mBlackJack;
    }

    boolean isSurrender() {
        return mSurrender;
    }

    boolean isWait() {
        return mWait;
    }

    boolean isBust() {
        return calculateValue() > BlackJackUtils.BLACK_JACK_VALUE;
    }

    void surrender() {
        mBet /= DOUBLE_RATIO;
        mSurrender = true;
    }

    int calculateValue() {
        return BlackJackUtils.calculateValue(mCards);
    }

    int getBet() {
        return mBet;
    }

    boolean isActive() {
        return !isWait() && !isBust() && !isBlackJack() && !isSurrender()
                && calculateValue() < BlackJackUtils.BLACK_JACK_VALUE;
    }

    List<BlackJackOperation> getAvailableOperations(int playerBalance) {
        List<BlackJackOperation> result = new ArrayList<>();
        if (canHit()) {
            result.add(BlackJackOperation.HIT);
        }
        result.add(BlackJackOperation.STAND);
        if (playerBalance >= mBet * 2 && canDoubleDown()) {
            result.add(BlackJackOperation.DOUBLE_DOWN);
        }
        if (playerBalance >= mBet * 2 && canSplit()) {
            result.add(BlackJackOperation.SPLIT);
        }
        result.add(BlackJackOperation.SURRENDER);
        return result;
    }

    void process(BlackJackOperation operation, BlackJackDeck deck) {
        if (operation == BlackJackOperation.HIT) {
            addCard(deck.getCard());
        } else if (operation == BlackJackOperation.DOUBLE_DOWN) {
            doubleDown(deck.getCard());
        } else if (operation == BlackJackOperation.STAND) {
            stand();
        } else if (operation == BlackJackOperation.SURRENDER) {
            surrender();
        }
    }

    boolean justInitialized() {
        return mCards.size() == 2;
    }

    void printInfo(boolean showHandNumber, int number, boolean results, int dealerValue) {
        BlackJackUtils.printPlayerHandValueInfoHeader(showHandNumber, number, mCards);
        for (int i = 0; i < mCards.size(); i++) {
            Card playerCard = mCards.get(i);
            if (i > 0) {
                System.out.print("; ");
            }
            System.out.print(playerCard + " ");
        }
        if (!results || !showHandNumber) {
            printSummary();
        } else {
            printResult(dealerValue);
        }
        System.out.println();
    }

    private void printSummary() {
        if (justInitialized() && calculateValue() == BlackJackUtils.BLACK_JACK_VALUE) {
            System.out.println("\nYou have BlackJack!");
        } else if (calculateValue() == BlackJackUtils.BLACK_JACK_VALUE) {
            System.out.println("\nYour stand");
        } else if (isSurrender()) {
            System.out.println("\nYour surrender");
        } else if (isBust()) {
            System.out.println("\nBust!");
        } else if (isWait() || calculateValue() == BlackJackUtils.BLACK_JACK_VALUE) {
            System.out.println("\nYour stand");
        } else {
            System.out.println();
        }
    }

    private void printResult(int dealerValue) {
        if (calculateValue() == BlackJackUtils.BLACK_JACK_VALUE) {
            System.out.println("\n $" + mBet);
        } else if (mSurrender || isBust()) {
            System.out.println("\n -$" + mBet);
        } else if (calculateValue() >= dealerValue || dealerValue > BlackJackUtils.BLACK_JACK_VALUE) {
            System.out.println("\n $" + mBet);
        } else {
            System.out.println("\n -$" + mBet);
        }
    }
}
