package com.lookren.blackjack.model;

public enum CardType {
    TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN,
    KNAVE, QUEEN, KING, ACE
}
