package com.lookren.blackjack.model;

public class Card {
    public final CardType mCardType;
    public final Suit mSuit;

    public Card(CardType cardType, Suit suit) {
        mCardType = cardType;
        mSuit = suit;
    }

    @Override
    public String toString() {
        return mCardType.name() + " " + mSuit.name();
    }
}