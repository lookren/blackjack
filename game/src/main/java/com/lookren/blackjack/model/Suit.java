package com.lookren.blackjack.model;

public enum Suit {
    HEARTS, SPADES, DIAMONDS, CLUBS
}