package com.lookren.blackjack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Game {
    public static final int INIT_BET = 10;
    private static final int INIT_BALANCE = 100;
    private static final int DECK_AMOUNT = 2;

    private static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws IOException {
        int playerBalance = INIT_BALANCE;
        BlackJackDeck deck = new BlackJackDeck(DECK_AMOUNT);
        int count = 0;
        boolean newDeck = false;
        while (true) {
            if (newDeck) {
                deck = new BlackJackDeck(DECK_AMOUNT);
                newDeck = false;
            }
            if (playerBalance < INIT_BET) {
                System.out.println("Not enough balance. Bye!");
                System.exit(0);
            } else {
                if (!deck.isReadyForGame()) {
                    System.out.println("Game over, reshuffle is needed.");
                    newDeck = true;
                    count = 0;
                    continue;
                }
                printBalanceInfoAndStart(playerBalance, count);
            }

            BlackJackPlayField field = new BlackJackPlayField();
            playerBalance += field.playGame(playerBalance, deck, in);
            count++;
        }
    }

    private static void printBalanceInfoAndStart(int playerBalance, int gameNumber) throws IOException {
        System.out.println("******************");
        System.out.println("Your balance: $" + playerBalance);
        System.out.println("One hand bet: $" + INIT_BET);
        System.out.println();
        System.out.println(gameNumber == 0 ? "Start play? (Y/N): " : "Play again? (Y/N): ");
        exitIfNo();
    }

    private static void exitIfNo() throws IOException {
        boolean entered = false;
        String enter = in.readLine();
        while (!entered) {
            if (enter != null && (enter.equals("N") || enter.equals("n"))) {
                System.out.print("\nBye!");
                entered = true;
                System.exit(0);
            } else if (enter != null && !enter.equals("Y") && !enter.equals("y")) {
                System.err.println("\nIncorrect enter. Please enter again:");
                enter = in.readLine();
            } else {
                entered = true;
            }
        }
    }
}