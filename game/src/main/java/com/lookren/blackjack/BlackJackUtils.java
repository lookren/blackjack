package com.lookren.blackjack;

import com.lookren.blackjack.model.Card;
import com.lookren.blackjack.model.CardType;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

public class BlackJackUtils {
    public static final int BLACK_JACK_VALUE = 21;

    static int calculateValue(List<Card> cards) {
        int result = 0;
        int aceCount = 0;
        for (Card each : cards) {
            if (each.mCardType == CardType.ACE) {
                aceCount++;
            }
            result += getValue(each.mCardType);
        }
        while (result > BLACK_JACK_VALUE && aceCount > 0) {
            result -= 10;
            aceCount--;
        }
        return result;
    }

    static int getValue(CardType cardType) {
        switch (cardType) {
            case TWO:
                return 2;
            case THREE:
                return 3;
            case FOUR:
                return 4;
            case FIVE:
                return 5;
            case SIX:
                return 6;
            case SEVEN:
                return 7;
            case EIGHT:
                return 8;
            case NINE:
                return 9;
            case TEN:
            case KNAVE:
            case QUEEN:
            case KING:
                return 10;
            case ACE:
                return 11;
            default:
                throw new IllegalArgumentException("unsupported CardType");
        }
    }

    static void printResult(int amount) {
        if (amount > 0) {
            System.out.println();
            System.out.println("Congratulations! You won $" + amount);
            System.out.println();
        } else {
            System.out.println();
            System.out.println("You lost $" + Math.abs(amount));
            System.out.println();
        }
    }

    static int getDealerValue(List<Card> dealerCards, boolean hideDealerCards) {
        return hideDealerCards ? getValue(dealerCards.get(0).mCardType)
                : calculateValue(dealerCards);
    }

    static void printGameFieldInfo(List<Card> dealerCards, List<PlayerHand> playerHands, boolean hideDealerCards, boolean first) {
        System.out.println("Dealer's value: " + getDealerValue(dealerCards, hideDealerCards));
        for (int i = 0; i < dealerCards.size(); i++) {
            Card dealerCard = dealerCards.get(i);
            if (i == 0) {
                System.out.print(dealerCard + " ");
            } else if (hideDealerCards) {
                System.out.print("; ? ");
            } else {
                System.out.print("; " + dealerCard + " ");
            }
        }
        if (hideDealerCards && first) {
            CardType visibleCardType = dealerCards.get(0).mCardType;
            if (visibleCardType == CardType.ACE || visibleCardType == CardType.KNAVE
                    || visibleCardType == CardType.QUEEN || visibleCardType == CardType.KING) {
                System.out.println("\nMaybe the dealer has BlackJack");
            }
        } else if (!hideDealerCards && dealerCards.size() == 2 && getDealerValue(dealerCards, false) == BLACK_JACK_VALUE) {
            System.out.println("\nThe dealer has BlackJack!");
            return;
        }
        for (int handIndex = 0; handIndex < playerHands.size(); handIndex++) {
            playerHands.get(handIndex).printInfo(playerHands.size() > 1, handIndex,
                    !hideDealerCards, getDealerValue(dealerCards, false));
        }
    }

    static BlackJackOperation getEnteredOperation(BufferedReader in, List<BlackJackOperation> expectedValues,
                                                  boolean showHandIndex, int handIndex) throws IOException {
        StringBuilder out = new StringBuilder();
        for (BlackJackOperation operation : expectedValues) {
            out.append(operation.toString());
        }
        System.out.println((showHandIndex ? "Hand#" + (handIndex + 1) + " operations: " : "") + out);
        while (true) {
            try {
                String value = in.readLine();
                int intValue = Integer.parseInt(value);
                for (BlackJackOperation each : expectedValues) {
                    if (intValue == each.getValue()) {
                        return each;
                    }
                }
            } catch (NumberFormatException nfe) {}
            System.err.println("Invalid Format! Enter again\n" + out);
        }
    }

    static void printPlayerHandValueInfoHeader(boolean showHandNumber, int number, List<Card> cards) {
        System.out.println("\n\nYour" + (showHandNumber ? " Hand#" + (number + 1) : "")
                + " value: " + calculateValue(cards));
    }
}
